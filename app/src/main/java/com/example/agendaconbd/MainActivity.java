package com.example.agendaconbd;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {


    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private AgendaContactos db;
    private Contacto savedContact;
    private long id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre= (EditText) findViewById(R.id.txtNombre);
        edtTelefono= (EditText) findViewById(R.id.txtTel1);
        edtTelefono2= (EditText) findViewById(R.id.txtTel2);
        edtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        edtNotas =(EditText) findViewById(R.id.txtNota);
        cbxFavorito = (CheckBox)findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        db = new AgendaContactos(MainActivity.this);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtNombre.getText().toString().equals("")||
                        edtDireccion.getText().toString().equals("")||
                        edtTelefono.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.msgerror,
                            Toast.LENGTH_SHORT).show();
                }else{

                    String n= edtNombre.getText().toString();
                    String d= edtDireccion.getText().toString();
                    String not= edtNotas.getText().toString();
                    String tel1= edtTelefono.getText().toString();
                    String tel2= edtTelefono2.getText().toString();
                    long idd= new Random().nextInt();

                    if(cbxFavorito.isChecked()){
                        int fav = 1;
                        db = new AgendaContactos(MainActivity.this);
                        Contacto c= new Contacto(idd,n,tel1,tel2,d, not,fav);
                        db.openDataBase();
                        long idx = db.insertarContacto(c);
                        Toast.makeText(MainActivity.this,"Contacto ingresado",Toast.LENGTH_SHORT).show();

                        db.cerrar();


                    }else{
                        int fav=0;
                        db = new AgendaContactos(MainActivity.this);
                        Contacto c= new Contacto(idd,n,tel1,tel2,d, not,fav);
                        db.openDataBase();
                        long idx = db.insertarContacto(c);
                        Toast.makeText(MainActivity.this,"Contacto ingresado",Toast.LENGTH_SHORT).show();
                        db.cerrar();


                    }

                }
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);

                startActivityForResult(i,0);

            }
        });


    }

    public void limpiar()
    {
        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Activity.RESULT_OK == resultCode) {
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id= contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            edtTelefono.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtDireccion.setText(contacto.getDomicilio());
            edtNotas.setText(contacto.getNotas());
            if (contacto.getFavorito() > 0) {
                cbxFavorito.setChecked(true);
            }

        } else {
            limpiar();
        }

    }




}
